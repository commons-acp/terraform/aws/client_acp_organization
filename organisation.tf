
resource "aws_organizations_organizational_unit" "clients" {
  name      = var.organization_name
  parent_id = var.parent_aws_organizations_organizational_unit_id
}
resource "aws_organizations_account" "client" {
  parent_id = aws_organizations_organizational_unit.clients.id
  name  = var.organization_name
  email = "walid.mansia+${var.organization_name}@portago.eu"

}

resource "aws_organizations_policy_attachment"  "client"{
  policy_id = var.aws_organizations_policy_id
  target_id = aws_organizations_account.client.id
}
output "organization_id" {
  value = aws_organizations_account.client.id
}


