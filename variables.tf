// organisation vars
variable "organization_name" {
  description = "the organization Name"
}
variable "parent_aws_organizations_organizational_unit_id" {
  description = "The parent morganization unit id"
}
variable "aws_organizations_policy_id" {
  description = "the client organizations policy id"
}
//group vars
variable "aws_region" {
  description = "AWS region"
}
